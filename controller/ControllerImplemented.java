package com.koval.controller;

import com.koval.model.*;
import java.util.LinkedList;
import java.util.List;

/**
 * Realization of the controller
 */
public class ControllerImplemented implements Controller {

    private Model model;
    private List<Remedy> bucket;

    public ControllerImplemented(){
        model = new Logic();
        bucket = new LinkedList<>();
    }

    @Override
    public List<Remedy> getRemedies() {
        return model.getRemediesList();
    }

    @Override
    public String getStatistics() {
        return model.getStatistics();
    }

    @Override
    public List<Remedy> getBucket(){
        return bucket;
    }

    @Override
    public void addToBucket(Remedy remedy){
        bucket.add(remedy);
    }

    @Override
    public void deleteFromBucket(Remedy remedy){
        bucket.remove(remedy);
    }

    @Override
    public void showBucket(){
        for (Remedy element: bucket) {
            System.out.println(element.toString());
        }
    }
}